package com.example.bf_2.chatapp.ui.Adapters;

import android.content.Context;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.bf_2.chatapp.R;
import com.example.bf_2.chatapp.models.sm.ChatMessage;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by BF-2 on 23/03/2017.
 */

public class ChatListAdapter extends BaseAdapter {

    private List<ChatMessage> chatMessages;
    private Context context;
    //   public static final SimpleDateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat("h:mm a", Locale.US);

    public ChatListAdapter(List<ChatMessage> chatMessages, Context context) {
        this.chatMessages = chatMessages;
        this.context = context;

    }
public void setData(ChatMessage chatMessage){
    if (this.chatMessages == null) {
        this.chatMessages = new ArrayList<>();
    }
    this.chatMessages.add(chatMessages.size(), chatMessage);
    Handler mainHandler = new Handler(context.getMainLooper());
    Runnable myRunnable = new Runnable() {
        @Override
        public void run() {
//            context.numComments.setText(posts.size() == 0 ? "0" : posts.size() + "");
            notifyDataSetChanged();

        } // This is your code
    };
    mainHandler.post(myRunnable);
}

    @Override
    public int getCount() {
        return chatMessages.size();
    }

    @Override
    public Object getItem(int position) {
        return chatMessages.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = null;
        ChatMessage message = chatMessages.get(position);
        ViewHolder1 holder1;
        if (message.getUserSendId() == 0) {
            if (convertView == null) {
                v = LayoutInflater.from(context).inflate(R.layout.chat_user1_item, null, false);
                holder1 = new ViewHolder1();
                holder1.messageTextView = (TextView) v.findViewById(R.id.textview_message);
                holder1.timeTextView = (TextView) v.findViewById(R.id.textview_time);

                v.setTag(holder1);
            } else {
                v = convertView;
                holder1 = (ViewHolder1) v.getTag();

            }

            holder1.messageTextView.setText(message.getText());

            holder1.timeTextView.setText((message.getDate()));

        } else {

            if (convertView == null) {
                v = LayoutInflater.from(context).inflate(R.layout.chat_user2_item, null, false);

                holder1 = new ViewHolder1();


                holder1.messageTextView = (TextView) v.findViewById(R.id.textview_message);
                holder1.timeTextView = (TextView) v.findViewById(R.id.textview_time);
//                holder2.messageStatus = (ImageView) v.findViewById(R.id.user_reply_status);
                v.setTag(holder1);

            } else {
                v = convertView;
                holder1 = (ViewHolder1) v.getTag();

            }

            holder1.messageTextView.setText(message.getText());


            holder1.timeTextView.setText((message.getDate()));

        }


        return v;
    }


    private class ViewHolder1 {
        public TextView messageTextView;
        public TextView timeTextView;


    }

    private class ViewHolder2 {
        public ImageView messageStatus;
        public TextView messageTextView;
        public TextView timeTextView;

    }
}
