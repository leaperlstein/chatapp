package com.example.bf_2.chatapp.ui.Activitis;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.bf_2.chatapp.ChatAppApplication;
import com.example.bf_2.chatapp.R;
import com.example.bf_2.chatapp.models.sm.ChatMessage;
import com.example.bf_2.chatapp.models.sm.Contacts;
import com.example.bf_2.chatapp.ui.Adapters.ContactsAdapter;
import com.example.bf_2.chatapp.ui.Fragments.ChatFragment;
import com.example.bf_2.chatapp.utils.ChatService;
import com.example.bf_2.chatapp.utils.General;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Observable;
import java.util.Observer;


public class MainActivity extends AppCompatActivity {

    public ChatFragment currentFragment;
    private List<Contacts> contactsList;
    private ContactsAdapter contactsAdapter;
    private ImageView notificationCircle;
    private TextView badge;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.action_bar);
        setContentView(R.layout.activity_main);
        setContactsList();
        setBadge();
        startService();
        ChatAppApplication.getInstance().getChatNotifications().addObserver(new Observer() {
            @Override
            public void update(Observable observable, Object chatMessage) {
                if (((ChatMessage) chatMessage).getUserSendId() == currentFragment.getId())
                    updateContacts((ChatMessage) chatMessage, true);
                else {
                    updateContacts((ChatMessage) chatMessage, false);
                    updateBadge(1);
                }
            }
        });
    }

    private void setBadge() {
        notificationCircle = (ImageView) getSupportActionBar().getCustomView().findViewById(R.id.notification_circle);
        badge = (TextView) getSupportActionBar().getCustomView().findViewById(R.id.num_circle);


    }

    private void startService() {
        Intent myIntent = new Intent(this, ChatService.class);
        sendBroadcast(myIntent);
    }

    public void setChatFragment(Contacts contacts) {
        currentFragment = new ChatFragment().newInstance(contacts);
        getSupportFragmentManager().beginTransaction().replace(R.id.chat_content, currentFragment).commit();
    }

    private void setContactsList() {
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.contacts_list);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        contactsList = createContacts();
        contactsAdapter = new ContactsAdapter(this, contactsList);
        recyclerView.setAdapter(contactsAdapter);
    }

    private List<Contacts> createContacts() {
        List<Contacts> contactsList = new ArrayList<Contacts>();
        for (int i = 0; i < 10; i++) {
            Contacts contacts = new Contacts();
            contacts.setId(i + 1);
            contacts.setName("contact  " + i);
            contacts.setText(General.random());
            contacts.setRead(true);
            contacts.setDate(General.convertDateToString(new Date()));
            contactsList.add(contacts);

        }
        setChatFragment(contactsList.get(0));
        return contactsList;
    }

    public void updateContacts(ChatMessage chat, boolean isRead) {
        for (Contacts contacts : contactsList) {
            if (contacts.getId() == chat.getUserSendId()) {
                contacts.setText(chat.getText());
                contacts.setRead(isRead);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        contactsAdapter.notifyDataSetChanged();

                    }
                });
            }
        }
    }

    public void updateBadge(final int num) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                int badgeNum;
                if (badge.getText().toString().trim().isEmpty())
                    badgeNum = 0;
                else
                    badgeNum = Integer.parseInt(badge.getText().toString());
                badgeNum += num;
                if (badgeNum > 0) {
                    notificationCircle.setVisibility(View.VISIBLE);
                    badge.setText(badgeNum + "");
                } else {
                    badge.setText("");
                    notificationCircle.setVisibility(View.INVISIBLE);
                }
            }
        });

    }

}
