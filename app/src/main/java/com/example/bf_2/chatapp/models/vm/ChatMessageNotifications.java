package com.example.bf_2.chatapp.models.vm;

import com.example.bf_2.chatapp.models.sm.ChatMessage;

import java.util.Observable;

public class ChatMessageNotifications extends Observable {
        public void newNotificationCome(ChatMessage chatMessage) {
            setChanged();
            notifyObservers(chatMessage);
        }
    }