package com.example.bf_2.chatapp;

import android.app.Application;

import com.example.bf_2.chatapp.models.vm.ChatMessageNotifications;

/**
 * Created by BF-2 on 26/03/2017.
 */

public class ChatAppApplication extends Application {

    private static ChatAppApplication instance;

    public static ChatAppApplication getInstance() {
        return instance;
    }
    public ChatMessageNotifications getChatNotifications() {
        return chatMessageNotifications;
    }
    public void sesChatNotifications(ChatMessageNotifications chatMessageNotifications) {
        this.chatMessageNotifications = chatMessageNotifications;
    }
    private ChatMessageNotifications chatMessageNotifications = new ChatMessageNotifications();

    @Override
    public void onCreate() {
        super.onCreate();
        ChatAppApplication.instance = this;
    }
}

