package com.example.bf_2.chatapp.utils;

import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.TimeZone;

import static android.content.Context.ACTIVITY_SERVICE;

/**
 * Created by BF-2 on 27/09/2016.
 */
public class General {

    public static String getPackageNameTopActivity(Context context) {
        ActivityManager am = (ActivityManager) context.getSystemService(ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
        Log.d("topActivity", "CURRENT Activity ::" + taskInfo.get(0).topActivity.getClassName());
        ComponentName componentInfo = taskInfo.get(0).topActivity;
        return taskInfo.get(0).topActivity.getClassName();
    }

    public static String convertDateFromServer(String date) {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        try {
            Date d = sdf.parse(date);
            SimpleDateFormat sdf1 = new SimpleDateFormat("dd/MM/yyyy");
//            return sdf1.format(General.convertUtcToLocal(d));
            return sdf1.format(d);
        } catch (ParseException ex) {
        }
        return null;
    }

    public static Date convertUtcToLocal(Date gmtTime) {
        // Convert UTC to Local Time
        long ts = System.currentTimeMillis();
        Date localTime = new Date(ts);
        Date fromGmt = new Date(gmtTime.getTime() + TimeZone.getDefault().getOffset(localTime.getTime()));
        return fromGmt;
    }

    public static String convertDateToString(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

        String d = sdf.format(date);
        return d;
    }

    public static String random() {
        Random generator = new Random();
        StringBuilder randomStringBuilder = new StringBuilder();
        int randomLength = generator.nextInt(40);
        char tempChar;
        for (int i = 0; i < randomLength; i++) {
            tempChar = (char) (generator.nextInt(96) + 32);
            randomStringBuilder.append(tempChar);
        }
        return randomStringBuilder.toString();
    }
}
