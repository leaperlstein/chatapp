package com.example.bf_2.chatapp.ui.Adapters;

import android.annotation.TargetApi;
import android.graphics.Typeface;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.bf_2.chatapp.R;
import com.example.bf_2.chatapp.models.sm.Contacts;
import com.example.bf_2.chatapp.ui.Activitis.MainActivity;
import com.example.bf_2.chatapp.utils.General;

import java.util.List;

public class ContactsAdapter extends RecyclerView.Adapter<ContactsAdapter.ViewHolder> {
    public List<Contacts> contactsList;
    MainActivity context;

    public ContactsAdapter(MainActivity context, List<Contacts> contactsList) {
        this.contactsList = contactsList;

        this.context = context;
    }

    public void setData(List<Contacts> contactsList) {
        this.contactsList = contactsList;
        notifyDataSetChanged();

    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView date;
        public TextView name;
        public TextView text;
        public View view;


        public ViewHolder(View v) {
            super(v);
            date = (TextView) v.findViewById(R.id.date);
            name = (TextView) v.findViewById(R.id.name);
            text = (TextView) v.findViewById(R.id.text);
            view = v;
        }
    }


    @Override
    public ContactsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_contacts, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        if (!contactsList.get(position).isRead())//&& (contactsList.get(position).getUserSendId() != 0
            holder.text.setTypeface(null, Typeface.BOLD);
        else holder.text.setTypeface(null, Typeface.NORMAL);
        holder.date.setText(General.convertDateFromServer(contactsList.get(position).getDate()));
        holder.name.setText(contactsList.get(position).getName());
        holder.text.setText(contactsList.get(position).getText());
        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!contactsList.get(position).isRead()) {
                    contactsList.get(position).setRead(true);
                    context.updateBadge(-1);
                    notifyDataSetChanged();
                }
                (context).setChatFragment(contactsList.get(position));

            }
        });
    }

    @Override
    public int getItemCount() {
        return contactsList.size();
    }

}