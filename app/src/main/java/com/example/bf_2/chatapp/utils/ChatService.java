package com.example.bf_2.chatapp.utils;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.NotificationCompat;

import com.example.bf_2.chatapp.ChatAppApplication;
import com.example.bf_2.chatapp.R;
import com.example.bf_2.chatapp.models.sm.ChatMessage;
import com.example.bf_2.chatapp.ui.Activitis.MainActivity;

import java.util.Date;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public class ChatService extends BroadcastReceiver {
    Context context;

    private ChatMessage createChat() {
        ChatMessage chatMessage = new ChatMessage();
        chatMessage.setUserSendId(randomId());
        chatMessage.setText(General.random());
        chatMessage.setDate(General.convertDateToString(new Date()));
        return chatMessage;

    }

    private void createNotification(ChatMessage chatMessage) {
        NotificationCompat.Builder localBuilder = new NotificationCompat.Builder(context);
        localBuilder.setAutoCancel(true)
                .setDefaults(Notification.DEFAULT_ALL)
                .setWhen(System.currentTimeMillis())
                .setContentTitle( chatMessage.getUserSendId()-1 +"קבלת הודעה חדשה מCONTACT ")
                .setSmallIcon(R.drawable.ic_icon)
                .setContentInfo(chatMessage.getText());
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(context.NOTIFICATION_SERVICE);
        notificationManager.notify(1, localBuilder.build());
    }

    private int randomId() {
        Random random = new Random();
        return random.nextInt(10) + 1;
    }

    @Override
    public void onReceive(final Context context, Intent intent) {
        this.context = context;
        new Timer().scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                if (General.getPackageNameTopActivity(context).equals(MainActivity.class.getName())) {
                    ChatMessage chatMessage = createChat();
                    createNotification(chatMessage);
                    ChatAppApplication.getInstance().getChatNotifications().newNotificationCome(chatMessage);
                }
            }
        }, 0, 100000);



    }
}