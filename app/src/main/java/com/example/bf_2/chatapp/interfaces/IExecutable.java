package com.example.bf_2.chatapp.interfaces;

/**
 * Created by BF-2 on 27/09/2016.
 */
public interface IExecutable<T> {
    public void execute(T data);
}

