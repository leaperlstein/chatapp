package com.example.bf_2.chatapp.ui.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.example.bf_2.chatapp.ChatAppApplication;
import com.example.bf_2.chatapp.R;
import com.example.bf_2.chatapp.models.sm.ChatMessage;
import com.example.bf_2.chatapp.models.sm.Contacts;
import com.example.bf_2.chatapp.ui.Adapters.ChatListAdapter;
import com.example.bf_2.chatapp.utils.General;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.Random;

public class ChatFragment extends Fragment {

    private static final String ID = "id";
    private static final String TEXT = "text";
    private static final String NAME = "name";
    public int id;
    private String text;
    private String name;
    private View view;
    private List<ChatMessage> chatMessageList;
    private ChatListAdapter chatAdapter;
    private ListView chatList;


    public ChatFragment() {
    }

    public static ChatFragment newInstance(Contacts contacts) {
        ChatFragment fragment = new ChatFragment();
        Bundle args = new Bundle();
        args.putInt(ID, contacts.getId());
        args.putString(NAME, contacts.getName());
        args.putString(TEXT, contacts.getText());
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            id = getArguments().getInt(ID);
            name = getArguments().getString(NAME);
            text = getArguments().getString(TEXT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_chat, container, false);
        setName();
        setChatList();
        TextView send = (TextView) view.findViewById(R.id.send);
        send.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    sendChat();
                }
                return false;
            }
        });
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendChat();
            }
        });
        ChatAppApplication.getInstance().getChatNotifications().addObserver(new Observer() {
            @Override
            public void update(Observable observable, Object chatMessage) {
                updateChat((ChatMessage) chatMessage);
            }
        });
        return view;
    }

    private void sendChat() {
        final EditText txt = (EditText) view.findViewById(R.id.text_body);
        if (!txt.getText().toString().trim().isEmpty()) {
            ChatMessage chatMessage = new ChatMessage();
            chatMessage.setChatId(chatMessageList.size() + 1);
            chatMessage.setUserSendId(0);
            chatMessage.setText(txt.getText().toString());
            chatMessage.setDate(General.convertDateToString(new Date()));
            chatMessageList.add(chatMessage);
            chatAdapter.notifyDataSetChanged();
            chatList.setSelection(chatMessageList.size() - 1);
            txt.setText("");
        }
    }

    private void setName() {
        TextView contactName = (TextView) view.findViewById(R.id.name);
        contactName.setText(name);
    }

    private void setChatList() {
        chatList = (ListView) view.findViewById(R.id.chat_list);
        chatMessageList = createChatMessages();
        chatAdapter = new ChatListAdapter(chatMessageList, getActivity());
        chatList.setAdapter(chatAdapter);
        chatList.setSelection(chatMessageList.size() - 1);
    }

    private List<ChatMessage> createChatMessages() {
        List<ChatMessage> chatMessageList = new ArrayList<ChatMessage>();
        for (int i = 0; i < 9; i++) {
            ChatMessage chatMessage = new ChatMessage();
            chatMessage.setChatId(i);
            chatMessage.setUserSendId(randomId());
            chatMessage.setText(General.random());
            chatMessage.setDate(General.convertDateToString(new Date()));
            chatMessageList.add(chatMessage);

        }
        ChatMessage chatMessage = new ChatMessage();
        chatMessage.setChatId(9);
        chatMessage.setUserSendId(randomId());
        chatMessage.setText(text);
        chatMessage.setDate(General.convertDateToString(new Date()));
        chatMessageList.add(chatMessage);
        return chatMessageList;
    }

    private int randomId() {
        Random random = new Random();
        return random.nextInt(2) == 1 ? id : 0;
    }

    public void updateChat(ChatMessage chat) {
        try {
            if (chatAdapter != null && chat.getUserSendId() == id) {
                chatAdapter.setData(chat);
                chatList.setSelection(chatMessageList.size() - 1);
//                updateContacts(chat,true);

            } else {
//                updateContacts(chat,context,false);
//                updateBadge(context,1);


            }
        } catch (Exception e) {

        }
    }
}
